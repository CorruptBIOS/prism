# Build container

FROM gradle:4.10.2-jdk11-slim AS build

COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

# Run container

FROM openjdk:16-ea-23-jdk-oraclelinux8 AS runtime

WORKDIR /opt/avaire/

RUN adduser --disabled-password --gecos '' prisma; \
    chown prisma:prisma -R /opt/prisma; \
    chmod u+w /opt/prisma; \
    chmod 0755 -R /opt/prisma

USER prisma

COPY --from=build /home/gradle/src/Prisma.jar /bin/

CMD ["java","-jar","/bin/Prisma.jar","-env","--use-plugin-index"]
